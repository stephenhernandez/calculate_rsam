.. calculate_rsam documentation master file, created by
   sphinx-quickstart on Mon Sep  2 07:51:26 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Calculate_rsam's documentation
==========================================

Library to calculate the RSAM (Real-time Seismic Amplitude Measurement) of seismic 
waveforms to use as input for an volcanic alert system

Description
-----------
The code retrieves waveform data of a station, calculates the rms and 
stores it in a MSSQL DB to be plotted by SAM.  


Requirements
-------------
The following packages must be installed 

.. code-block:: bash

    $ conda install obspy 
    $ conda install pyodbc 
    $ conda install json 


Execution
---------
To execute the module run the following command

.. code-block:: bash

    $ cd calculate_rsam
    $ python ./run_rsam.py CONFIGURATION_FILE.TXT

To view the logs, run the following command

.. code-block:: bash

    $ cd calculate_rsam
    $ tail -f ./rsam.log

Testing
-------
The test can be executed with the following command

.. code-block:: bash

    $ cd calculate_rsam 
    $ python -m pytest -v test/test_rsam.py



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rsam

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
