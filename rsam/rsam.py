from rsam import db_connection

from obspy.signal import filter
import numpy as np
import logging

from obspy.clients.arclink import Client as clientArclink
from obspy.clients.seedlink import Client as clientSeedlink
from obspy.clients.filesystem.sds import Client as clientArchive

def calculate_rms(stream_data):
    """
    Calculate rms of a time series
    
    :param numpy.ndarray stream_data: data of an obspy stream object 
    
    """
    data_np64=np.array(stream_data,dtype='float64')
    square_data_np64=np.square(data_np64)
    sum_data=square_data_np64.sum()
    media_data=sum_data/len(data_np64)
    rms=media_data**(0.5)
    
    return rms


def choose_service(server_parameter_dict):
    """
    Function that connects to a mseed data server (ARCLINK|SEEDLINK|ARCHIVE) and returns the respective client
    
    :param dict server_parameter_dict: dictionary with the parameters of the MSEED server
    :returns: obspy.client object
    :raises Exception e: General exception if there is any problem to connect 
    """

    if server_parameter_dict['name'] == 'ARCLINK':
        try:
            return clientArclink(server_parameter_dict['user'],server_parameter_dict['serverIP'],int(server_parameter_dict['port']))
        except Exception as e:
            logging.error("Error trying  Arclink : %s %s %s" %(server_parameter_dict['user'],server_parameter_dict['serverIP'],server_parameter_dict['port']) )
            raise Exception("Error Arclink : %s -- %s %s %s" %(str(e), server_parameter_dict['user'],server_parameter_dict['serverIP'],server_parameter_dict['port']) )

    elif server_parameter_dict['name'] == 'SEEDLINK':
        try:
            
            return clientSeedlink(server_parameter_dict['serverIP'],int(server_parameter_dict['port']),timeout=5 )
        except Exception as e:
            logging.error("Error trying  Seedlink : %s %s" %(server_parameter_dict['serverIP'],server_parameter_dict['port']) )
            raise Exception("Error Seedlink :%s -- %s %s" %(str(e),server_parameter_dict['serverIP'],server_parameter_dict['port']) )

    elif server_parameter_dict['name'] == 'ARCHIVE':
        try:
            return clientArchive(server_parameter_dict['archivePath'])
        except Exception as e:
            logging.error("Error trying  Archive : %s " %(server_parameter_dict['archivePath']) )
            raise Exception("Error Archive : %s -- %s" %(str(e),server_parameter_dict['archivePath']))


def fill_zeros(station,channel,diaUTC):
    """
    This function fills the rmsData dictionary with zeros if no data could be retrieved from server. 
    :param string station: station's name
    :param string channel: channel's name
    :param UTCDateTime diaUTC: datetime object 
    """
    rmsData['rsam_estacion']=station
    rmsData['rsam_canal']=channel
    rmsData['rsam_fecha_proceso']=str(diaUTC.datetime)
    rmsData['rms']=0
    rmsData['rsam_banda1']=0
    rmsData['rsam_banda2']=0
    rmsData['rsam_banda3']=0
    rmsData['rsam_banda4']=0   
    rmsData['rsam_banda5']=0
        
def fill_rms(station_stream,dUTC):
    
    """
    This function receives an obspy data stream to process and fill the rsam global dictionary\
    If calculated values of filtered bands are greater than rms fill it with zero
    
    :param obspy.stream station_stream: Stream object of a station-channel to calculate RMS
    :param UTCDateTime dUTC: Datetime object. 
    """
    
    station_stream.detrend()
    rmsData['rsam_estacion']=station_stream[0].stats['station']
    rmsData['rsam_canal']=station_stream[0].stats['channel']
    rmsData['rsam_fecha_proceso']=str(dUTC.datetime)
    
    rmsData['rms']=round(calculate_rms(station_stream[0].data),4)

    dataFilt1=filter.bandpass(station_stream[0].data,0.05,0.125,station_stream[0].stats['sampling_rate'])
    rmsData['rsam_banda1']=round(calculate_rms(dataFilt1),4)
    
    dataFilt2=filter.bandpass(station_stream[0].data,2,8,station_stream[0].stats['sampling_rate'])
    rmsData['rsam_banda2']=round(calculate_rms(dataFilt2),4)

    dataFilt3=filter.bandpass(station_stream[0].data,0.25,2,station_stream[0].stats['sampling_rate'])
    rmsData['rsam_banda3']=round(calculate_rms(dataFilt3),4)

    dataFilt4=filter.highpass(station_stream[0].data,10.0,station_stream[0].stats['sampling_rate'],corners=1,zerophase=True)
    rmsData['rsam_banda4']=round(calculate_rms(dataFilt4),4)
    
    dataFilt5=filter.bandpass(station_stream[0].data,0.5,5,station_stream[0].stats['sampling_rate'])
    rmsData['rsam_banda5']=round(calculate_rms(dataFilt5),4)
    
    if rmsData['rsam_banda1'] > rmsData['rms']:
        rmsData['rsam_banda1']=0.0
    if rmsData['rsam_banda2'] > rmsData['rms']:
        rmsData['rsam_banda2']=0.0
    if rmsData['rsam_banda3'] > rmsData['rms']:
        rmsData['rsam_banda3']=0.0
    if rmsData['rsam_banda4'] > rmsData['rms']:
        rmsData['rsam_banda4']=0.0
    if rmsData['rsam_banda5'] > rmsData['rms']:
        rmsData['rsam_banda5']=0.0



def get_stream(service,conSrv,net,cod,loc,cha,dUTC,win):
    """
    This function retrieves waveform data of a station and channel 
    
    :param string service: Name of the server to connect to. 
    :param obspy.client conSrv: client to connect to Arclink, Seedlink or Sdsarchive server
    :param string net,cod,loc,cha: parameters of a station
    :param UTCDateTime dUTC: datetime to recover data 
    """
    logging.info('Starting station: %s %s %s %s' %(net,cod,loc,cha))
    if service=='ARCLINK':
        try:
            streamStat=conSrv.get_waveforms(net,cod,loc,cha,dUTC,dUTC + win,route=False,compressed=False)
            return streamStat
        except Exception as e:
            logging.info("Error getting streamStat: %s" %str(e))
    else:
        try:
            streamStat=conSrv.get_waveforms(net,cod,loc,cha,dUTC,dUTC + win)
            return streamStat
        except Exception as e:
            logging.info("Error getting streamStat: %s" %str(e))


def rsam(stations,dUTC,service,conSrv,conDB,mode,win):
    """
    Recover a 60 secs stream, calculate RSAM and store in DB
    
    :param dict stations: dictionary of the stations to process
    :param UTCDateTime dUTC: datetime to recover data
    :param string service: name of the server to connect to
    :param pyodbc conDB: DB client 
    :param string mode: Choose INSERT or UPDATE
    :param int win: windows time to recover data. 
    """
    
    for stname,sta in stations.items():
        for loc in sta['loc']:
            for cha in sta['cha']:
                streamStat=get_stream(service, conSrv, sta['net'],sta['cod'],loc,cha,dUTC,win)
                
                if streamStat != None:
                    if len(streamStat)>0:
                        logging.info("Starting calculateRSAM()")
                        logging.info(streamStat)
                        fill_rms(streamStat,dUTC)
                    else:
                        logging.info("insertar 0")
                        fill_zeros(sta['cod'], cha, dUTC)
                    logging.debug(rmsData)                
                    if mode=='INSERT':
                        db_connection.insertRow(conDB, rmsData)
                    elif mode=='UPDATE':
                        db_connection.updateRow(conDB, rmsData)
                else:
                    continue


def rsamUpdate_relleno(stations,diaUTC,service,conSrv,conDB,win,periodo):
    """
    Recover a win secs stream, and call sliceStream
    """
    for sta in stations.itervalues():
        for loc in sta['loc']:
            for cha in sta['cha']:
                logging.info('%s,%s,%s,%s,%s' %(sta['net'],sta['cod'],loc,cha,diaUTC))
                
                streamStation=get_stream(service,conSrv,sta['net'],sta['cod'],loc,cha,diaUTC,win)
                if streamStation !=None and len(streamStation)>0:
                    logging.info("Start division in minutes")
                    logging.info(streamStation)
                    sliceStream(streamStation,diaUTC,periodo,conDB)
                    
                else:
                    logging.info("No existen datos")
                    #fill_zeros(sta['cod'], sta['cod'], )

def sliceStream(str_sta,start_time,period,conDB):
    """
    Slice a stream, calculate rms and then store in DB
    """
   
    
    for x in range(0,86400/60):
        slice_start_time=start_time+(60*x)
        end_time=start_time+(60*(x+1))

        pass
        
        temp_stream=str_sta.slice(slice_start_time,end_time)
        if len(temp_stream)==0:
            logging.info("Gap in data. Fill with zeros")
            logging.info(str_sta[0].stats['station'], str_sta[0].stats['channel'], slice_start_time)
            fill_zeros(str_sta[0].stats['station'], str_sta[0].stats['channel'], slice_start_time)
        else:
            fill_rms(temp_stream,slice_start_time)
        ##Imprimir datos para dandrade 
        print("%s,%s,%s" %(rmsData['rsam_estacion'],rmsData['rsam_fecha_proceso'],rmsData['rsam_banda4']))
        #db_connection.updateRow(conDB,rmsData)
        #db_connection.insertRow(conDB,rmsData)


rmsData={'rsam_estacion':'','rsam_canal':'','rsam_fecha_proceso':'','rms':'','rsam_banda1':'','rsam_banda2':'','rsam_banda3':'','rsam_banda4':'','rsam_banda5':''} 







