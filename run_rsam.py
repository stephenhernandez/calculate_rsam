
import os,sys
import logging
from obspy.core import UTCDateTime

from rsam import db_connection
from rsam import rsam_utils
from rsam import rsam


def main():
    """
    This script parse the configuration text file, set the parameters and runs the rsam modules 
    """
    
    exec_dir=os.path.realpath(os.path.dirname(__file__))
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',filename=os.path.join(exec_dir,"rsam.log"),level=logging.INFO)
    
    is_error=False
    
    if len(sys.argv)==1:
        is_error=True
    
    else:   
        try:
            run_param=rsam_utils.read_parameters(sys.argv[1])
        
        except Exception as e:
            logging.error("Error reading configuration sets in file: %s" %(str(e)))
            raise Exception("Error reading configuration file: %s" %(str(e)))
            
        try:
            DB_ID=run_param['DATABASE']['id']
            db_config_file= run_param['DATABASE']['file_path']
            db_mode=run_param['DATABASE']['mode']
            mseedID=run_param['MSEED_SERVER']['id']
            mseed_server_config_file=run_param['MSEED_SERVER']['file_path']
            station_file=run_param['STATION_FILE']['file_path']
        
        except Exception as e:
            logging.error("Error getting parameters: %s" %str(e))
            raise Exception("Error getting parameters: %s" %str(e))
        
        try:
            db_param=rsam_utils.read_config_file(db_config_file)
            mseed_server_param=rsam_utils.read_config_file(mseed_server_config_file)
            stations_dict=rsam_utils.read_config_file(station_file)
        except Exception as e:
            logging.error("Error reading configuration file: %s" %str(e))
            raise Exception("Error reading configuration file: %s" %str(e))
        
        try:
            mseed_client=rsam.choose_service(mseed_server_param[mseedID])
            
        except Exception as e:
            logging.error("Failed to create mseed client")
            raise Exception("Failed to create mseed client")
        
        try:
            db_client=db_connection.createConexionDB(db_param[DB_ID]['host'],db_param[DB_ID]['port'],db_param[DB_ID]['user'],db_param[DB_ID]['pass'],db_param[DB_ID]['DBName'])
        except Exception as e:
            logging.error("Failed to create database client")
            raise Exception("Failed to create database client")
            
            
        try:  
            diaUTC=UTCDateTime(UTCDateTime.now().strftime("%Y-%m-%d %H:%M:00")) - 300
            rsam.rsam(stations_dict,diaUTC,mseedID,mseed_client,db_client,db_mode,60)     
        except Exception as e:
            logging.error("Error calculating rsam: %s" %str(e))
            raise Exception("Error calculating rsam: %s" %str(e))

    
    if is_error:
        logging.info(f'Usage: python {sys.argv[0]} configuration_file.txt ')
        print(f'Usage: python {sys.argv[0]} CONFIGURATION_FILE.txt ')    

main()


