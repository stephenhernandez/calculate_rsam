
from rsam import rsam
from obspy import read

data_file="./test/EC.APED..HNZ.2016.107_1908.mseed"

rsam_calculated=1077222.10462

def test_rsam_calculate_rsam():
    'Test the calculate_rms method'
 
    data=read(data_file)
    assert round(rsam.calculate_rms(data[0].data),5)==rsam_calculated
    

    
