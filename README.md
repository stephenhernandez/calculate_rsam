Code to calculate the RSAM (Real-time Seismic Amplitude Measurement) of seismic waveforms to use as input for an volcanic alert system

Please visit the documentation in the following URL:

https://wacero.gitlab.io/calculate_rsam/
